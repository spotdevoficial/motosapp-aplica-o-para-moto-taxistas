
angular.module('app', ['ionic', 'app.controllers', 'app.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'login/login.html'
    })

    .state('signup', {
        url: '/signup',
        controller: 'SignupCtrl',
        templateUrl: 'signup/signup.html'
    })

    .state('signupUser', {
        url: '/signup/user',
        controller: 'SignupCtrl',
        templateUrl: 'signup/signup_user.html'
    })

    .state('signupMoto', {
        url: '/signup/moto',
        controller: 'SignupCtrl',
        templateUrl: 'signup/signup_moto.html'
    })

    .state('recover', {
        url: '/recover',
        controller: 'RecoverCtrl',
        templateUrl: 'recover/recover.html'
    })

  $urlRouterProvider.otherwise('/signup/moto');

});
